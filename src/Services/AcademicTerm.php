<?php

namespace MiamiOH\AcademicTermWebService\Services;

use MiamiOH\Pike\App\Framework\RESTng\PikeServiceFactory;
use MiamiOH\Pike\App\Service\ViewTermService;
use MiamiOH\Pike\Domain\Collection\TermCollection;
use MiamiOH\Pike\Domain\Model\Term;
use MiamiOH\Pike\Exception\InvalidArgumentException;
use MiamiOH\Pike\Exception\NotFoundException;
use MiamiOH\Pike\Exception\TermNotFoundException;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;

class AcademicTerm extends Service
{
    /**
     * @var PikeServiceFactory
     */
    private $pike;
    /**
     * @var ViewTermService
     */
    private $viewTermService;
    /**
     * @var string
     */
    private $datasource_name = 'MUWS_SEC_PROD';

    /**
     * @var Request
     */
    private $request;

    /**
     * @var array
     */
    private $options;

    /**
     * @var Response
     */
    private $response;

    private function setup()
    {
        $this->request = $this->getRequest();
        $this->response = $this->getResponse();
        $this->options = $this->request->getOptions();
    }


    public function setPikeServiceFactory(PikeServiceFactory $pikeServiceFactory)
    {
        $this->pike = $pikeServiceFactory->getEloquentPikeServiceMapper($this->datasource_name);
        $this->viewTermService = $this->pike->getViewTermService();
    }

    public function getAcademicTerms()
    {
        $this->setup();

        // Handle error for invalid next OR previous option
        $termId = null;
        $prev = null;
        $next = null;

        if (isset($this->options['termId'])) {
            $this->validateTermId($this->options['termId']);
            $termId = $this->options['termId'];
        }

        if (isset($this->options['numOfFutureTerms'])) {
            $this->validatePositiveInteger($this->options['numOfFutureTerms']);
            $next = $this->options['numOfFutureTerms'];
        }

        if (isset($this->options['numOfPastTerms'])) {
            $this->validatePositiveInteger($this->options['numOfPastTerms']);
            $prev = $this->options['numOfPastTerms'];
        }

        if ($prev || $next) {
            if ($prev) {
                try {
                    $termCollection = $this->viewTermService->getPrevTerms($prev,
                        $termId);
                    $this->addTermCollectionToPayload($termCollection);
                } catch (\Exception $e) {
                    $this->addToPayload([]);
                }
            }


            try {
                if ($termId === null) {
                    $currentTerm = $this->viewTermService->getCurrentTerm();
                    $this->addToPayload([$this->termToArray($currentTerm)]);
                } else {
                    $term = $this->viewTermService->getByCode($termId);
                    $this->addToPayload([$this->termToArray($term)]);
                }
            } catch (\Exception $e) {
                $this->addToPayload([]);
            }

            if ($next) {
                try {
                    $termCollection = $this->viewTermService->getNextTerms($next,
                        $termId);
                    $this->addTermCollectionToPayload($termCollection);
                } catch (\Exception $e) {
                    $this->addToPayload([]);
                }
            }
        } else {
            if ($termId === null) {
                $termCollection = $this->viewTermService->getAll();

                $this->addTermCollectionToPayload($termCollection);
            } else {
                try {
                    $term = $this->viewTermService->getByCode($termId);
                    $this->addTermCollectionToPayload(new TermCollection([$term]));
                } catch (\Exception $e) {
                    $this->addToPayload([]);
                }
            }
        }

        return $this->response;
    }

    public function getAcademicTerm()
    {
        $this->setup();

        $termId = $this->request->getResourceParam('termId');

        try {
            $this->validateTermId($termId);
            $term = $this->viewTermService->getByCode($termId);
            $this->addTermToPayload($term);
        } catch (TermNotFoundException $e) {
            $this->response->setStatus(App::API_NOTFOUND);
        } catch (BadRequest $e) {
            $this->response->setPayload([
                'message' => $e->getMessage()
            ]);
            $this->response->setStatus(App::API_BADREQUEST);
        }

        return $this->response;
    }

    public function currentAcademicTerm()
    {
        $this->setup();

        try {
            $term = $this->viewTermService->getCurrentTerm();
            $this->addTermToPayload($term);
        } catch (TermNotFoundException $e) {
            $this->response->setStatus(App::API_NOTFOUND);
        }

        return $this->response;
    }

    public function nextAcademicTerm()
    {
        $this->setup();

        try {
            $term = $this->viewTermService->getNextTerm();
            $this->addTermToPayload($term);
        } catch (TermNotFoundException $e) {
            $this->response->setStatus(App::API_NOTFOUND);
        }

        return $this->response;
    }

    public function previousAcademicTerm()
    {
        $this->setup();

        try {
            $term = $this->viewTermService->getPrevTerm();
            $this->addTermToPayload($term);
        } catch (TermNotFoundException $e) {
            $this->response->setStatus(App::API_NOTFOUND);
        }

        return $this->response;
    }

    private function addToPayload(array $data)
    {
        $payload = $this->response->getPayload();

        $payload = array_merge($payload, $data);

        $this->response->setPayload($payload);
    }

    private function addTermToPayload(Term $term)
    {
        $this->addToPayload($this->termToArray($term));
    }

    private function termToArray(Term $term): array
    {
        return [
            'termId' => (string)$term->getCode(),
            'name' => $term->getDescription(),
            'startDate' => $term->getStartDate()->format('Y-m-d'),
            'endDate' => $term->getEndDate()->format('Y-m-d'),
            'displayTerm' => $term->isDisplayed()
        ];
    }

    private function termCollectionToArray(TermCollection $termCollection): array
    {
        $data = [];

        foreach ($termCollection as $term) {
            $data[] = $this->termToArray($term);
        }

        return $data;
    }

    private function addTermCollectionToPayload(TermCollection $termCollection)
    {
        $this->addToPayload($this->termCollectionToArray($termCollection));
    }

    private function validateTermId(string $termId)
    {
        if (!preg_match('/^[0-9]{4}(10|15|20|30)$/', $termId)) {
            throw new BadRequest('query parameter, "termId", must be <4-digit-year>{10|15|20|30}, e.g. 201710, 201815, 201720, 201330');
        }
    }

    private function validatePositiveInteger(string $input)
    {
        if ((string)intval($input) !== $input || intval($input) < 0) {
            throw new BadRequest('query parameter, "numOfFutureTerms" and "numOfPastTerms", must be a positive integer');
        }
    }
}
