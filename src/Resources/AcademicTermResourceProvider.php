<?php

namespace MiamiOH\AcademicTermWebService\Resources;

use MiamiOH\RESTng\App;

class AcademicTermResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{

    private $serviceName = 'AcademicTerm';
    private $tag = "academicTerm";
    private $resourceRoot = "academicTerm.v2";
    private $patternRoot = "/academicTerm/v2";
    private $classPath = 'MiamiOH\AcademicTermWebService\Services\AcademicTerm';

    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => $this->tag,
            'description' => 'Banner Academic Term Web Services'
        ));

        $this->addDefinition(array(
            'name' => 'AcademicTerm',
            'type' => 'object',
            'properties' => array(
                'termId' => array(
                    'type' => 'string',
                ),
                'name' => array(
                    'type' => 'string',
                ),
                'startDate' => array(
                    'type' => 'string',
                ),
                'endDate' => array(
                    'type' => 'string',
                ),
                'displayTerm' => array(
                    'type' => 'boolean',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'AcademicTerm.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/AcademicTerm'
            )
        ));

        $this->addDefinition(array(
            'name' => 'AcademicTerm.Exception',
            'type' => 'object',
            'properties' => array(
                'message' => array(
                    'type' => 'string',
                ),
                'line' => array(
                    'type' => 'integer',
                ),
                'file' => array(
                    'type' => 'string',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'AcademicTerm.Exception.404',
            'type' => 'object',
            'properties' => array()
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => $this->serviceName,
            'class' => $this->classPath,
            'description' => 'Banner Academic Term Web Services',
            'set' => array(
                'pikeServiceFactory' => array(
                    'type' => 'service',
                    'name' => 'PikeServiceFactory'
                ),
            ),
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => $this->resourceRoot,
            'description' => '- Get a collection of academic terms by 3 optional filters. Three filters are all optional. If no  ' .
                'filter provided, then all term codes records returned. If numOfFutureTerms or numOfPastTerms  ' .
                "provided with termId parameter blank, then current term code is used.\n" .
                '- When numOfFutureTerms or numOfPastTerms is entered, the number of records returned are number ' .
                "entered in the field plus one, the current term record.\n" .
                '- In the gap of two terms, the next term is returned.',
            'summary' => 'Get a collection of academic terms by 3 optional filters',
            'pattern' => $this->patternRoot,
            'service' => $this->serviceName,
            'tags' => [$this->tag],
            'method' => 'getAcademicTerms',
            'returnType' => 'collection',
            'options' => array(
                'termId' => array(
                    'required' => false,
                    'description' => 'The term to be queried. Default term is the current term'
                ),
                'numOfFutureTerms' => array(
                    'required' => false,
                    'description' => 'Number of future term records to return'
                ),
                'numOfPastTerms' => array(
                    'required' => false,
                    'description' => 'Number of past term records to return'
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A collection of academic terms',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/AcademicTerm.Collection',
                    )
                ),
                App::API_BADREQUEST => array(
                    'description' => 'Server could not understand the request due to invalid syntax',
                    'returns' => array(
                        'type' => 'object',
                        '$ref' => '#/definitions/AcademicTerm.Exception',
                    )
                )
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => $this->resourceRoot . '.current',
            'description' => 'Get current academic term. In the gap of two terms, the next term is returned.',
            'summary' => 'Get current academic term',
            'pattern' => $this->patternRoot . '/currentTerm',
            'service' => $this->serviceName,
            'tags' => [$this->tag],
            'method' => 'currentAcademicTerm',
            'returnType' => 'model',
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Current academic term',
                    'returns' => array(
                        'type' => 'object',
                        '$ref' => '#/definitions/AcademicTerm',
                    )
                ),
                App::API_NOTFOUND => array(
                    'description' => 'Current date does not belong to any academic term',
                    'returns' => array(
                        'type' => 'object',
                        '$ref' => '#/definitions/AcademicTerm.Exception.404',
                    )
                )
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => $this->resourceRoot . '.next',
            'description' => 'Get next academic term of current term.',
            'summary' => 'Get next academic term of current term',
            'pattern' => $this->patternRoot . '/nextTerm',
            'service' => $this->serviceName,
            'tags' => [$this->tag],
            'method' => 'nextAcademicTerm',
            'returnType' => 'model',
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Next academic term of current term',
                    'returns' => array(
                        'type' => 'object',
                        '$ref' => '#/definitions/AcademicTerm',
                    )
                ),
                App::API_NOTFOUND => array(
                    'description' => 'Next academic term of current term does not exist',
                    'returns' => array(
                        'type' => 'object',
                        '$ref' => '#/definitions/AcademicTerm.Exception.404',
                    )
                )
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => $this->resourceRoot . '.previous',
            'description' => 'Get previous academic term of current term.',
            'summary' => 'Get previous academic term of current term',
            'pattern' => $this->patternRoot . '/previousTerm',
            'service' => $this->serviceName,
            'tags' => [$this->tag],
            'method' => 'previousAcademicTerm',
            'returnType' => 'model',
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Previous academic term of current term',
                    'returns' => array(
                        'type' => 'object',
                        '$ref' => '#/definitions/AcademicTerm',
                    )
                ),
                App::API_NOTFOUND => array(
                    'description' => 'Previous academic term of current term does not exist',
                    'returns' => array(
                        'type' => 'object',
                        '$ref' => '#/definitions/AcademicTerm.Exception.404',
                    )
                )
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => $this->resourceRoot . '.getByCode',
            'description' => 'Get an academic term with user specified term id',
            'pattern' => $this->patternRoot . '/:termId',
            'service' => $this->serviceName,
            'tags' => [$this->tag],
            'method' => 'getAcademicTerm',
            'returnType' => 'model',
            'params' => array(
                'termId' => array('description' => 'unique academic term code'),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Academic term with user specified term id',
                    'returns' => array(
                        'type' => 'object',
                        '$ref' => '#/definitions/AcademicTerm',
                    )
                ),
                App::API_NOTFOUND => array(
                    'description' => 'Academic term with user specified term id does not exist',
                    'returns' => array(
                        'type' => 'object',
                        '$ref' => '#/definitions/AcademicTerm.Exception.404',
                    )
                ),
                App::API_BADREQUEST => array(
                    'description' => 'Invalid syntax of user specified term id',
                    'returns' => array(
                        'type' => 'object',
                        '$ref' => '#/definitions/AcademicTerm.Exception',
                    )
                )
            )
        ));
    }

    public function registerOrmConnections(): void
    {

    }
}