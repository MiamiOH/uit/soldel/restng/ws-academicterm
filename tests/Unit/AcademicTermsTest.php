<?php
/**
 * Author: liaom
 * Date: 4/20/18
 * Time: 3:50 PM
 */

use Carbon\Carbon;
use MiamiOH\AcademicTermWebService\Services\AcademicTerm;
use MiamiOH\Pike\App\Framework\RESTng\PikeServiceFactory;
use MiamiOH\Pike\App\Mapper\AppMapper;
use MiamiOH\Pike\App\Service\ViewTermService;
use MiamiOH\Pike\Domain\Collection\TermCollection;
use MiamiOH\Pike\Domain\Model\Term;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Exception\TermNotFoundException;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class AcademicTermTest
 */
class AcademicTermsTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $api;
    /**
     * @var AcademicTerm
     */
    private $academicTerm;
    /**
     * @var MockObject
     */
    private $request;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var MockObject
     */
    private $viewTermService;

    /**
     * @var MockObject
     */
    private $pike;
    /**
     * @var MockObject
     */
    private $pikeServiceFactory;

    /**
     * @var Term
     */
    private $term1;
    /**
     * @var Term
     */
    private $term2;
    /**
     * @var Term
     */
    private $term3;

    /**
     * @var array
     */
    private $termPayload1;
    /**
     * @var array
     */
    private $termPayload2;
    /**
     * @var array
     */
    private $termPayload3;

    private function pass()
    {
        $this->assertTrue(true);
    }

    protected function setUp()
    {
        parent::setUp();
        $this->request = $this->getMockBuilder(Request::class)
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();
        $this->academicTerm = new AcademicTerm();

        $this->viewTermService = $this->getMockBuilder(ViewTermService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->pike = $this->createMock(AppMapper::class);
        $this->pike->method('getViewTermService')->willReturn($this->viewTermService);

        $this->pikeServiceFactory = $this->createMock(PikeServiceFactory::class);
        $this->pikeServiceFactory->method('getEloquentPikeServiceMapper')->willReturn($this->pike);

        $this->academicTerm->setPikeServiceFactory($this->pikeServiceFactory);
        $this->academicTerm->setRequest($this->request);

        $this->term1 = new Term(
            new TermCode('201710'),
            'term 201710',
            Carbon::create(2017, 8, 24, 0, 0, 0),
            Carbon::create(2017, 12, 15, 0, 0, 0),
            true
        );

        $this->term2 = new Term(
            new TermCode('201715'),
            'term 201715',
            Carbon::create(2017, 12, 24, 0, 0, 0),
            Carbon::create(2018, 1, 15, 0, 0, 0),
            true
        );

        $this->term3 = new Term(
            new TermCode('000000'),
            'term 000000',
            Carbon::create(2099, 8, 24, 0, 0, 0),
            Carbon::create(2099, 12, 15, 0, 0, 0),
            false
        );

        $this->termPayload1 = $this->termToTermPayload($this->term1);
        $this->termPayload2 = $this->termToTermPayload($this->term2);
        $this->termPayload3 = $this->termToTermPayload($this->term3);
    }

    private function termToTermPayload(Term $term): array
    {
        return [
            'termId' => $term->getCode(),
            'name' => $term->getDescription(),
            'startDate' => $term->getStartDate()->format('Y-m-d'),
            'endDate' => $term->getEndDate()->format('Y-m-d'),
            'displayTerm' => $term->isDisplayed()
        ];
    }

    public function testSendingInvalidTermIdToGetAcademicTermsWillCauseBadRequestException(
    )
    {
        try {
            $this->request->method('getOptions')->willReturn(['termId' => '201725']);
            $this->academicTerm->getAcademicTerms();
            $this->fail();
        } catch (BadRequest $e) {
            $this->pass();
        }

        try {
            $this->request->method('getOptions')->willReturn(['termId' => 'afasdf']);
            $this->academicTerm->getAcademicTerms();
            $this->fail();
        } catch (BadRequest $e) {
            $this->pass();
        }
    }

    public function testSendingInvalidNextFilterToGetAcademicTermsWillCauseBadRequestException(
    )
    {
        try {
            $this->request->method('getOptions')->willReturn(['numOfFutureTerms' => 'afasdf']);
            $this->academicTerm->getAcademicTerms();
            $this->fail();
        } catch (BadRequest $e) {
            $this->pass();
        }
        try {
            $this->request->method('getOptions')->willReturn(['numOfFutureTerms' => -3]);
            $this->academicTerm->getAcademicTerms();
            $this->fail();
        } catch (BadRequest $e) {
            $this->pass();
        }
    }

    public function testSendingInvalidPreviousFilterToGetAcademicTermsWillCauseBadRequestException(
    )
    {
        try {
            $this->request->method('getOptions')->willReturn(['numOfPastTerms' => '-2']);
            $this->academicTerm->getAcademicTerms();
            $this->fail();
        } catch (BadRequest $e) {
            $this->pass();
        }

        try {
            $this->request->method('getOptions')->willReturn(['numOfPastTerms' => 'afasdf']);
            $this->academicTerm->getAcademicTerms();
            $this->fail();
        } catch (BadRequest $e) {
            $this->pass();
        }
    }

    public function testGetAcademicTermsWithoutAnyFilterShouldReturnAllTerms()
    {
        $this->request->method('getOptions')->willReturn([]);
        $this->viewTermService->method('getAll')->willReturn(new TermCollection([
            $this->term1,
            $this->term2
        ]));

        $response = $this->academicTerm->getAcademicTerms();

        $this->assertEquals([$this->termPayload1, $this->termPayload2],
            $response->getPayload());
    }

    public function testGetAcademicTermsWithOnlyTermIdFilterWillReturnACollectionWithOneTerm(
    )
    {
        $this->request->method('getOptions')->willReturn(['termId' => $this->term1->getCode()]);
        $this->viewTermService->method('getByCode')->willReturn($this->term1);

        $response = $this->academicTerm->getAcademicTerms();

        $this->assertEquals([$this->termPayload1], $response->getPayload());
    }

    public function testGetAcademicTermsWithOnlyInvalidTermIdWillReturnEmptyArray()
    {
        $this->request->method('getOptions')->willReturn(['termId' => '201710']);
        $this->viewTermService->method('getByCode')->willThrowException(new \Exception());

        $response = $this->academicTerm->getAcademicTerms();

        $this->assertEquals([], $response->getPayload());
    }

    public function testGetAcademicTermsWithTermIdAndNextFilter()
    {
        $this->request->method('getOptions')->willReturn([
            'termId' => $this->term1->getCode(),
            'numOfFutureTerms' => '1'
        ]);
        $this->viewTermService->method('getNextTerms')->willReturn(new TermCollection([$this->term2]));
        $this->viewTermService->method('getByCode')->willReturn($this->term1);


        $response = $this->academicTerm->getAcademicTerms();

        $this->assertEquals([$this->termPayload1, $this->termPayload2],
            $response->getPayload());
    }

    public function testGetAcademicTermsWithInvalidTermIdAndNextFilter()
    {
        $this->request->method('getOptions')->willReturn([
            'termId' => '209910',
            'numOfFutureTerms' => '1'
        ]);
        $this->viewTermService->method('getNextTerms')->willThrowException(new Exception());
        $this->viewTermService->method('getByCode')->willThrowException(new Exception());

        $response = $this->academicTerm->getAcademicTerms();

        $this->assertEquals([], $response->getPayload());
    }

    public function testGetAcademicTermsWithTermIdAndPreviousFilter()
    {
        $this->request->method('getOptions')->willReturn([
            'termId' => $this->term2->getCode(),
            'numOfPastTerms' => '1'
        ]);
        $this->viewTermService->method('getPrevTerms')->willReturn(new TermCollection([$this->term1]));
        $this->viewTermService->method('getByCode')->willReturn($this->term2);


        $response = $this->academicTerm->getAcademicTerms();

        $this->assertEquals([$this->termPayload1, $this->termPayload2],
            $response->getPayload());
    }

    public function testGetAcademicTermsWithInvalidTermIdAndPreviousFilter()
    {
        $this->request->method('getOptions')->willReturn([
            'termId' => '209910',
            'numOfPastTerms' => '1'
        ]);
        $this->viewTermService->method('getPrevTerms')->willThrowException(new Exception());
        $this->viewTermService->method('getByCode')->willThrowException(new Exception());

        $response = $this->academicTerm->getAcademicTerms();

        $this->assertEquals([], $response->getPayload());
    }

    public function testGetAcademicTermsWithTermIdNextAndPreviousFilter()
    {
        $this->request->method('getOptions')->willReturn([
            'termId' => $this->term2->getCode(),
            'numOfFutureTerms' => '1',
            'numOfPastTerms' => '1'
        ]);
        $this->viewTermService->method('getNextTerms')->willReturn(new TermCollection([$this->term3]));
        $this->viewTermService->method('getByCode')->willReturn($this->term2);
        $this->viewTermService->method('getPrevTerms')->willReturn(new TermCollection([$this->term1]));

        $response = $this->academicTerm->getAcademicTerms();

        $this->assertEquals([
            $this->termPayload1,
            $this->termPayload2,
            $this->termPayload3
        ],
            $response->getPayload());

    }

    public function testGetAcademicTermsWithOnlyNextAndPreviousFilter()
    {
        $this->request->method('getOptions')->willReturn([
            'numOfFutureTerms' => '1',
            'numOfPastTerms' => '1'
        ]);
        $this->viewTermService->method('getNextTerms')->willReturn(new TermCollection([$this->term3]));
        $this->viewTermService->method('getCurrentTerm')->willReturn($this->term2);
        $this->viewTermService->method('getPrevTerms')->willReturn(new TermCollection([$this->term1]));

        $response = $this->academicTerm->getAcademicTerms();

        $this->assertEquals([
            $this->termPayload1,
            $this->termPayload2,
            $this->termPayload3
        ],
            $response->getPayload());
    }

    public function testGetAcademicTermByTermId()
    {
        $this->request->method('getResourceParam')->willReturn($this->term1->getCode());
        $this->viewTermService->method('getByCode')->willReturn($this->term1);

        $response = $this->academicTerm->getAcademicTerm();

        $this->assertEquals($this->termPayload1,
            $response->getPayload());
    }

    public function testGetAcademicTermWithNonExistTermIdWillCauseNoFoundException()
    {
        $this->request->method('getResourceParam')->willReturn('209910');
        $this->viewTermService->method('getByCode')->willThrowException(new TermNotFoundException());
        $response = $this->academicTerm->getAcademicTerm();
        $this->assertEquals(App::API_NOTFOUND, $response->getStatus());

    }

    public function testCanGetCurrentTerm()
    {
        $this->viewTermService->method('getCurrentTerm')->willReturn($this->term1);

        $response = $this->academicTerm->currentAcademicTerm();

        $this->assertEquals($this->termPayload1, $response->getPayload());
    }

    public function testCurrentTermNotFound()
    {
        $this->viewTermService->method('getCurrentTerm')->willThrowException(new TermNotFoundException());
        $response = $this->academicTerm->currentAcademicTerm();
        $this->assertEquals(App::API_NOTFOUND, $response->getStatus());

    }

    public function testCanGetNextTerm()
    {
        $this->viewTermService->method('getNextTerm')->willReturn($this->term1);

        $response = $this->academicTerm->nextAcademicTerm();

        $this->assertEquals($this->termPayload1, $response->getPayload());
    }

    public function testNextTermNotFound()
    {
        $this->viewTermService->method('getNextTerm')->willThrowException(new TermNotFoundException());
        $response = $this->academicTerm->nextAcademicTerm();
        $this->assertEquals(App::API_NOTFOUND, $response->getStatus());

    }

    public function testCanGetPreviousTerm()
    {
        $this->viewTermService->method('getPrevTerm')->willReturn($this->term1);

        $response = $this->academicTerm->previousAcademicTerm();

        $this->assertEquals($this->termPayload1, $response->getPayload());
    }

    public function testPreviousNotFound()
    {
        $this->viewTermService->method('getPrevTerm')->willThrowException(new TermNotFoundException());
        $response = $this->academicTerm->previousAcademicTerm();
        $this->assertEquals(App::API_NOTFOUND, $response->getStatus());
    }

    public function testValidateTermId()
    {
        $reflectionAcademicTerms = new ReflectionClass($this->academicTerm);
        $reflectionValidateTermId = $reflectionAcademicTerms->getMethod('validateTermId');
        $reflectionValidateTermId->setAccessible(true);

        try {
            $reflectionValidateTermId->invoke($this->academicTerm, 'asdf');
            $this->fail();
        } catch (BadRequest $e) {
            $this->pass();
        }

        try {
            $reflectionValidateTermId->invoke($this->academicTerm, '3777777');
            $this->fail();
        } catch (BadRequest $e) {
            $this->pass();
        }

        try {
            $reflectionValidateTermId->invoke($this->academicTerm, '201725');
            $this->fail();
        } catch (BadRequest $e) {
            $this->pass();
        }
    }

    public function testValidatePositiveInteger()
    {
        $reflectionAcademicTerms = new ReflectionClass($this->academicTerm);
        $reflectionValidatePositiveInteger = $reflectionAcademicTerms->getMethod('validatePositiveInteger');
        $reflectionValidatePositiveInteger->setAccessible(true);

        try {
            $reflectionValidatePositiveInteger->invoke($this->academicTerm, 'asdf');
            $this->fail();
        } catch (BadRequest $e) {
            $this->pass();
        }

        try {
            $reflectionValidatePositiveInteger->invoke($this->academicTerm,
                '-23489');
            $this->fail();
        } catch (BadRequest $e) {
            $this->pass();
        }
    }

    public function testTermToArray()
    {
        $reflectionAcademicTerms = new ReflectionClass($this->academicTerm);
        $reflectionTermToArray = $reflectionAcademicTerms->getMethod('termToArray');
        $reflectionTermToArray->setAccessible(true);

        $return = $reflectionTermToArray->invoke($this->academicTerm, $this->term1);

        $this->assertEquals($this->termPayload1, $return);
    }

    public function testTermCollectionToArray()
    {
        $reflectionAcademicTerms = new ReflectionClass($this->academicTerm);
        $reflectionTermCollectionToArray = $reflectionAcademicTerms->getMethod('termCollectionToArray');
        $reflectionTermCollectionToArray->setAccessible(true);

        $return = $reflectionTermCollectionToArray->invoke($this->academicTerm,
            new TermCollection([$this->term1, $this->term2]));

        $this->assertEquals([$this->termPayload1, $this->termPayload2], $return);
    }
}