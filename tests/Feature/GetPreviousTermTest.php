<?php
/**
 * Author: liaom
 * Date: 7/23/18
 * Time: 5:18 PM
 */

namespace MiamiOH\AcademicTermWebService\Tests\Feature;


use Carbon\Carbon;
use MiamiOH\Pike\Exception\TermNotFoundException;
use MiamiOH\RESTng\App;

class GetPreviousTermTest extends TestCase
{
    public function testGetPreviousTerm() {
        $this->viewTermService
            ->method('getPrevTerm')
            ->willReturn(
                $this->mockTerm(
                    '201710',
                    'something',
                    Carbon::createFromFormat('Y-m-d', '2017-05-10'),
                    Carbon::createFromFormat('Y-m-d', '2018-06-20'),
                    true
                )
            );

        $response = $this->getJson('/academicTerm/v2/previousTerm');

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                'termId' => '201710',
                'name' => 'something',
                'startDate' => '2017-05-10',
                'endDate' => '2018-06-20',
                'displayTerm' => true
            ]
        ]);
    }

    public function testTermNotFound() {
        $this->viewTermService
            ->method('getPrevTerm')
            ->willThrowException(new TermNotFoundException());
        $response = $this->getJson('/academicTerm/v2/previousTerm');

        $response->assertStatus(App::API_NOTFOUND);
    }
}