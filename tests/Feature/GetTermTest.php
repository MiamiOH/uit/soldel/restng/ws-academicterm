<?php
/**
 * Author: liaom
 * Date: 7/23/18
 * Time: 4:04 PM
 */

namespace MiamiOH\AcademicTermWebService\Tests\Feature;

use Carbon\Carbon;
use MiamiOH\Pike\Domain\Collection\TermCollection;
use MiamiOH\Pike\Exception\InvalidArgumentException;
use MiamiOH\Pike\Exception\TermNotFoundException;
use MiamiOH\RESTng\App;

class GetTermTest extends TestCase
{
   public function testGetAllTerm() {
       $term1 = $this->mockTerm(
           '201710',
           'something',
           Carbon::createFromFormat('Y-m-d', '2017-05-10'),
           Carbon::createFromFormat('Y-m-d', '2018-06-20'),
           true
       );

       $term2 = $this->mockTerm(
           '201720',
           'something 2',
           Carbon::createFromFormat('Y-m-d', '2018-01-11'),
           Carbon::createFromFormat('Y-m-d', '2018-06-30'),
           false
       );

       $termCollection = new TermCollection([$term1, $term2]);

       $this->viewTermService
           ->method('getAll')
           ->willReturn($termCollection);

       $response = $this->getJson('/academicTerm/v2');

       $response->assertStatus(App::API_OK);
       $response->assertJson([
           'data' => [
               [
                   'termId' => '201710',
                   'name' => 'something',
                   'startDate' => '2017-05-10',
                   'endDate' => '2018-06-20',
                   'displayTerm' => true
               ],
               [
                   'termId' => '201720',
                   'name' => 'something 2',
                   'startDate' => '2018-01-11',
                   'endDate' => '2018-06-30',
                   'displayTerm' => false
               ]
           ]
       ]);
   }

   public function testGetByCode() {
       $this->viewTermService
           ->method('getByCode')
           ->with('201710')
           ->willReturn(
               $this->mockTerm(
                   '201710',
                   'something',
                   Carbon::createFromFormat('Y-m-d', '2017-05-10'),
                   Carbon::createFromFormat('Y-m-d', '2018-06-20'),
                   true
               )
           );

       $response = $this->getJson('/academicTerm/v2?termId=201710');

       $response->assertStatus(App::API_OK);
       $response->assertJson([
           'data' => [
               [
                   'termId' => '201710',
                   'name' => 'something',
                   'startDate' => '2017-05-10',
                   'endDate' => '2018-06-20',
                   'displayTerm' => true
               ]
           ]
       ]);
   }

   public function testTermNotFound() {
       $this->viewTermService
           ->method('getByCode')
           ->with('201710')
           ->willThrowException(new TermNotFoundException());

       $response = $this->getJson('/academicTerm/v2?termId=201710');

       $response->assertStatus(App::API_OK);
       $response->assertJson([
           'data' => []
       ]);
   }

   public function testGetTermByCodeAndWithNumOfFutureTerms() {
       $term1 = $this->mockTerm(
           '201710',
           'something',
           Carbon::createFromFormat('Y-m-d', '2017-05-10'),
           Carbon::createFromFormat('Y-m-d', '2018-06-20'),
           true
       );

       $term2 = $this->mockTerm(
           '201720',
           'something 2',
           Carbon::createFromFormat('Y-m-d', '2018-01-11'),
           Carbon::createFromFormat('Y-m-d', '2018-06-30'),
           false
       );

       $this->viewTermService
           ->method('getByCode')
           ->with('201710')
           ->willReturn($term1);

       $this->viewTermService
           ->method('getNextTerms')
           ->with(1)
           ->willReturn(new TermCollection([$term2]));

       $response = $this->getJson('/academicTerm/v2?termId=201710&numOfFutureTerms=1');

       $response->assertStatus(App::API_OK);
       $response->assertJson([
           'data' => [
               [
                   'termId' => '201710',
                   'name' => 'something',
                   'startDate' => '2017-05-10',
                   'endDate' => '2018-06-20',
                   'displayTerm' => true
               ],
               [
                   'termId' => '201720',
                   'name' => 'something 2',
                   'startDate' => '2018-01-11',
                   'endDate' => '2018-06-30',
                   'displayTerm' => false
               ]
           ]
       ]);
   }

    public function testGetTermByCodeAndWithNumOfPreviousTerms() {
        $term1 = $this->mockTerm(
            '201710',
            'something',
            Carbon::createFromFormat('Y-m-d', '2017-05-10'),
            Carbon::createFromFormat('Y-m-d', '2018-06-20'),
            true
        );

        $term2 = $this->mockTerm(
            '201720',
            'something 2',
            Carbon::createFromFormat('Y-m-d', '2018-01-11'),
            Carbon::createFromFormat('Y-m-d', '2018-06-30'),
            false
        );

        $this->viewTermService
            ->method('getByCode')
            ->with('201720')
            ->willReturn($term2);

        $this->viewTermService
            ->method('getPrevTerms')
            ->with(1)
            ->willReturn(new TermCollection([$term1]));

        $response = $this->getJson('/academicTerm/v2?termId=201720&numOfPastTerms=1');

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                [
                    'termId' => '201710',
                    'name' => 'something',
                    'startDate' => '2017-05-10',
                    'endDate' => '2018-06-20',
                    'displayTerm' => true
                ],
                [
                    'termId' => '201720',
                    'name' => 'something 2',
                    'startDate' => '2018-01-11',
                    'endDate' => '2018-06-30',
                    'displayTerm' => false
                ]
            ]
        ]);
    }

    public function testGetPreviousAndNextTermsWithoutSpecifyingTermId() {
        $term1 = $this->mockTerm(
            '201710',
            'something',
            Carbon::createFromFormat('Y-m-d', '2017-05-10'),
            Carbon::createFromFormat('Y-m-d', '2018-06-20'),
            true
        );

        $term2 = $this->mockTerm(
            '201720',
            'something 2',
            Carbon::createFromFormat('Y-m-d', '2018-01-11'),
            Carbon::createFromFormat('Y-m-d', '2018-06-30'),
            false
        );

        $term3 = $this->mockTerm(
            '201730',
            'something 3',
            Carbon::createFromFormat('Y-m-d', '2018-01-11'),
            Carbon::createFromFormat('Y-m-d', '2018-06-30'),
            true
        );

        $this->viewTermService
            ->method('getCurrentTerm')
            ->willReturn($term2);

        $this->viewTermService
            ->method('getPrevTerms')
            ->with(1)
            ->willReturn(new TermCollection([$term1]));

        $this->viewTermService
            ->method('getNextTerms')
            ->with(1)
            ->willReturn(new TermCollection([$term3]));

        $response = $this->getJson('/academicTerm/v2?numOfPastTerms=1&numOfFutureTerms=1');

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                [
                    'termId' => '201710',
                    'name' => 'something',
                    'startDate' => '2017-05-10',
                    'endDate' => '2018-06-20',
                    'displayTerm' => true
                ],
                [
                    'termId' => '201720',
                    'name' => 'something 2',
                    'startDate' => '2018-01-11',
                    'endDate' => '2018-06-30',
                    'displayTerm' => false
                ],
                [
                    'termId' => '201730',
                    'name' => 'something 3',
                    'startDate' => '2018-01-11',
                    'endDate' => '2018-06-30',
                    'displayTerm' => true
                ]
            ]
        ]);
    }
}