<?php
/**
 * Author: liaom
 * Date: 7/23/18
 * Time: 4:07 PM
 */

namespace MiamiOH\AcademicTermWebService\Tests\Feature;


use Carbon\Carbon;
use MiamiOH\Pike\App\Framework\RESTng\PikeServiceFactory;
use MiamiOH\Pike\App\Mapper\AppMapper;
use MiamiOH\Pike\App\Service\ViewTermService;
use MiamiOH\Pike\Domain\Model\Term;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use PHPUnit\Framework\MockObject\MockObject;

class TestCase extends \MiamiOH\RESTng\Testing\TestCase
{
    /**
     * @var MockObject
     */
    protected $viewTermService;

    protected function setUp()
    {
        parent::setUp();
        $pikeServiceFactory = $this->createMock(PikeServiceFactory::class);
        $this->viewTermService = $this->createMock(ViewTermService::class);
        $pike = $this->createMock(AppMapper::class);
        $pike->method('getViewTermService')->willReturn($this->viewTermService);
        $pikeServiceFactory->method('getEloquentPikeServiceMapper')->willReturn($pike);

        $this->app->useService([
            'name' => 'PikeServiceFactory',
            'object' => $pikeServiceFactory,
            'description' => 'Mocked Pike Service Factory'
        ]);
    }

    protected function mockTerm(
        string $code,
        string $description,
        Carbon $startDate,
        Carbon $endDate,
        bool $isDisplayed
    ): MockObject {
        $term = $this->createMock(Term::class);

        $term->method('getCode')->willReturn(new TermCode($code));
        $term->method('getDescription')->willReturn($description);
        $term->method('getStartDate')->willReturn($startDate);
        $term->method('getEndDate')->willReturn($endDate);
        $term->method('isDisplayed')->willReturn($isDisplayed);

        return $term;
    }
}