<?php
/**
 * Author: liaom
 * Date: 7/23/18
 * Time: 4:04 PM
 */

namespace MiamiOH\AcademicTermWebService\Tests\Feature;

use Carbon\Carbon;
use MiamiOH\Pike\Exception\InvalidArgumentException;
use MiamiOH\Pike\Exception\TermNotFoundException;
use MiamiOH\RESTng\App;

class GetByCodeTest extends TestCase
{
    public function testGetTermByCode() {
        $this->viewTermService
            ->method('getByCode')
            ->with('201710')
            ->willReturn(
                $this->mockTerm(
                    '201710',
                    'something',
                    Carbon::createFromFormat('Y-m-d', '2017-05-10'),
                    Carbon::createFromFormat('Y-m-d', '2018-06-20'),
                    true
                )
            );

        $response = $this->getJson('/academicTerm/v2/201710');

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                'termId' => '201710',
                'name' => 'something',
                'startDate' => '2017-05-10',
                'endDate' => '2018-06-20',
                'displayTerm' => true
            ]
        ]);
    }

    public function testCodeNotFound() {
        $this->viewTermService
            ->method('getByCode')
            ->with('201710')
            ->willThrowException(new TermNotFoundException());

        $response = $this->getJson('/academicTerm/v2/201710');

        $response->assertStatus(App::API_NOTFOUND);
    }

    public function testInvalidCode() {
        $this->viewTermService
            ->method('getByCode')
            ->with('ab')
            ->willThrowException(new InvalidArgumentException());

        $response = $this->getJson('/academicTerm/v2/ab');

        $response->assertStatus(App::API_BADREQUEST);
    }
}