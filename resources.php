<?php

return [
    'resources' => [
        'academicTerm' => [
            MiamiOH\AcademicTermWebService\Resources\AcademicTermResourceProvider::class,
        ],
    ]
];